<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

// tcbopen路由
$router->group(function () use ($router){
    $router->post('api/common/upload/image', '\app\uctoo\common\api\Upload@image');
    $router->post('api/common/upload/file', '\app\uctoo\common\api\Upload@file');
});