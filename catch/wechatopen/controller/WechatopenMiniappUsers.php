<?php

namespace catchAdmin\wechatopen\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\wechatopen\model\WechatopenMiniappUsers as WechatopenMiniappUsersModel;
use catcher\traits\UserHasGroupTrait;

class WechatopenMiniappUsers extends CatchController
{
    use UserHasGroupTrait;
    protected $WechatopenMiniappUsersModel;
    protected $user;
    
    public function __construct(WechatopenMiniappUsersModel $WechatopenMiniappUsersModel)
    {
        $this->user = $this->WechatopenMiniappUsersModel = $WechatopenMiniappUsersModel;
    }
    
    /**
     * 列表
     * @time 2021年09月03日 17:59
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->WechatopenMiniappUsersModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年09月03日 17:59
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->WechatopenMiniappUsersModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年09月03日 17:59
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->WechatopenMiniappUsersModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年09月03日 17:59
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->WechatopenMiniappUsersModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年09月03日 17:59
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->WechatopenMiniappUsersModel->deleteBy($id));
    }

    /**
     * 通过token获取小程序用户帐号
     * @time 2022年02月22日 12:34
     * @param $token
     */
    public function getUser(\think\Request $request)
    {
        $params = input('post.');
        $token = $request->header('token');
        $user = $this->WechatopenMiniappUsersModel->where('token','=',$token)->hidden(['session_key','password'])->find();
        return CatchResponse::success($user);
    }
}