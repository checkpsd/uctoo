<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class WechatopenMiniappUsers extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('wechatopen_miniapp_users', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '开放平台小程序用户' ,'id' => 'id' ,'primary_key' => ['id']]);
        $table->addColumn('user_ids', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '关联管理后台users表ID',])
			->addColumn('appid', 'string', ['limit' => 64,'null' => false,'default' => '','signed' => true,'comment' => '关联applet表appid',])
			->addColumn('openid', 'string', ['limit' => 64,'null' => false,'default' => '','signed' => true,'comment' => 'openid',])
			->addColumn('unionid', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '用户统一标识',])
			->addColumn('nickname', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '用户昵称',])
			->addColumn('phoneNumber', 'string', ['limit' => 18,'null' => true,'signed' => true,'comment' => '用户绑定的手机号（国外手机号会有区号）',])
			->addColumn('purePhoneNumber', 'string', ['limit' => 18,'null' => true,'signed' => true,'comment' => '没有区号的手机号',])
			->addColumn('countryCode', 'string', ['limit' => 6,'null' => true,'signed' => true,'comment' => '区号',])
			->addColumn('password', 'string', ['limit' => 32,'null' => true,'signed' => true,'comment' => '登录密码',])
			->addColumn('gender', 'boolean', ['null' => false,'default' => 0,'signed' => true,'comment' => '性别:1=男,2=女,0=未知',])
			->addColumn('city', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '城市',])
			->addColumn('province', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '省份',])
			->addColumn('country', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '国家',])
			->addColumn('avatarUrl', 'string', ['limit' => 256,'null' => true,'signed' => true,'comment' => '头像',])
			->addColumn('language', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '语言',])
			->addColumn('subscribe', 'boolean', ['null' => false,'default' => 1,'signed' => true,'comment' => '是否使用该小程序标识，0=未关注，1=关注',])
			->addColumn('subscribe_time', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => true,'comment' => '使用时间',])
			->addColumn('remark', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '运营者对粉丝的备注',])
			->addColumn('tagid_list', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '用户被打上的标签ID列表',])
			->addColumn('subscribe_scene', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '用户使用的渠道来源',])
			->addColumn('qr_scene', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '二维码扫码场景（开发者自定义）',])
			->addColumn('qr_scene_str', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '二维码扫码场景描述（开发者自定义）',])
			->addColumn('privilege', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '用户特权信息，json数组',])
			->addColumn('loginip', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => 'ip地址',])
			->addColumn('token', 'string', ['limit' => 32,'null' => true,'signed' => true,'comment' => 'token,自定义登录态请求api的标识，前端需缓存',])
			->addColumn('status', 'string', ['limit' => 32,'null' => true,'signed' => true,'comment' => '状态',])
			->addColumn('access_token', 'string', ['limit' => 256,'null' => true,'signed' => true,'comment' => 'access_token',])
			->addColumn('access_token_overtime', 'string', ['limit' => 11,'null' => false,'default' => 0,'signed' => true,'comment' => 'access_token过期时间',])
			->addColumn('session_key', 'string', ['limit' => 256,'null' => true,'signed' => true,'comment' => '会话密钥',])
			->addColumn('verification', 'string', ['limit' => 64,'null' => true,'signed' => true,'comment' => '验证',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除字段',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
			->addIndex(['openid'], ['unique' => true,'name' => 'wechatopen_miniapp_users_openid'])
            ->create();
    }
}
