<?php

namespace catchAdmin\system\model\search;

trait ConfigSearch
{
    public function searchKeyAttr($query, $value, $data)
    {
        return $query->where('key', $value);
    }
}
