<?php

namespace catchAdmin\minishop\model;

use catcher\base\CatchModel as Model;
/**
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $category_title
 * @property string $category_type
 * @property string $category_image
 * @property int $category_status
 * @property int $category_sort
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class MinishopProductCategory extends Model
{
    protected $pk = 'id';
    // 表名
    public $name = 'minishop_product_category';
    // 数据库字段映射
    public $field = array(
        'id',
        // 类目父ID
        'parent_id',
        // 类目名称
        'name',
        // 类目标题
        'category_title',
        // 类目类型
        'category_type',
        // 类目图片
        'category_image',
        // 类目状态
        'status',
        // 类目排序
        'sort',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    );


    protected $updateChildrenFields = 'status';

    /**
     * 列表数据
     *
     * @time 2020年01月09日
     * @return array
     * @throws DbException
     */
    public function getList(): array
    {
        return $this->catchSearch()
            ->catchOrder()
            ->select()->toTree();
    }

    /**
     * 获取子分类IDS
     *
     * @time 2020年11月04日
     * @param $id
     * @throws DbException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @return mixed
     */
    public static function getChildrenCategoryIds($id)
    {
        $categoryIds = MinishopProductCategory::field(['id', 'parent_id'])->select()->getAllChildrenIds([$id]);

        $categoryIds[] = $id;

        return $categoryIds;
    }
}