<?php

namespace catchAdmin\minishop\model;

use catcher\base\CatchModel as Model;
use catchAdmin\minishop\model\search\MinishopSpuSearch;
/**
 *
 * @property int $id
 * @property int $product_id
 * @property string $out_product_id
 * @property string $title
 * @property string $sub_title
 * @property string $head_img
 * @property string $desc_info
 * @property int $brand_id
 * @property int $status
 * @property int $edit_status
 * @property int $min_price
 * @property string $cats
 * @property string $attrs
 * @property string $model
 * @property string $shopcat
 * @property string $skus
 * @property int $sales_count
 * @property int $weigh
 * @property string $template_id
 * @property string $ext_json
 * @property string $memo
 * @property string $ext_attrs
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class MinishopSpu extends Model
{
    use MinishopSpuSearch;
    // 表名
    public $name = 'minishop_spu';
    // 数据库字段映射
    public $field = array(
        'id',
        // 小商店内部商品ID
        'product_id',
        // 商家自定义商品ID
        'out_product_id',
        // 标题
        'title',
        // 副标题
        'sub_title',
        // 主图,多张,列表
        'head_img',
        // 商品详情，图文
        'desc_info',
        // 商家需要申请品牌
        'brand_id',
        // 商品线上状态
        'status',
        // 商品草稿状态
        'edit_status',
        // 商品SKU最小价格（单位：分）
        'min_price',
        // 商家需要先申请可使用类目
        'cats',
        // 商品属性
        'attrs',
        // 商品型号
        'model',
        // 分类ID
        'shopcat',
        // 商品skus
        'skus',
        // 销量
        'sales_count',
        // 排序
        'weigh',
        // 模板id
        'template_id',
        // ext_json
        'ext_json',
        // 备注
        'memo',
        // 更多属性
        'ext_attrs',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    );

}