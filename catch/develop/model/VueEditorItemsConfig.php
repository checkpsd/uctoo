<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2022 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------
namespace catchAdmin\develop\model;

use catcher\base\CatchModel as Model;
/**
 *
 * @property int $id
 * @property string $type
 * @property string $category
 * @property string $name
 * @property string $value
 * @property int $page_id
 * @property string $schema
 * @property string $uiSchema
 * @property string $formData
 * @property string $errorSchema
 * @property string $formFooter
 * @property string $formProps
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class VueEditorItemsConfig extends Model
{
    
    public $field = [
        //
        'id',
        // 类型
        'type',
        // 分类
        'category',
        // 组件名称
        'name',
        // 组件配置json
        'value',
        // 归属页面ID
        'page_id',
        // schema
        'schema',
        // uiSchema
        'uiSchema',
        // formData
        'formData',
        // errorSchema
        'errorSchema',
        // formFooter
        'formFooter',
        // formProps
        'formProps',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    ];
    
    public $name = 'vue_editor_items_config';
}