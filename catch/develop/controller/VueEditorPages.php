<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2022 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------
namespace catchAdmin\develop\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\develop\model\VueEditorPages as VueEditorPagesModel;
use think\Response;

class VueEditorPages extends CatchController
{
    
    protected $vueEditorPagesModel;
    
    /**
     *
     * @time 2022/03/13 17:38
     * @param VueEditorPagesModel $vueEditorPagesModel
     * @return mixed
     */
    public function __construct(VueEditorPagesModel $vueEditorPagesModel)
    {
        $this->vueEditorPagesModel = $vueEditorPagesModel;
    }
    
    /**
     *
     * @time 2022/03/13 17:38
     * @return Response
     */
    public function index() : Response
    {
        return CatchResponse::paginate($this->vueEditorPagesModel->getList());
    }
    
    /**
     *
     * @time 2022/03/13 17:38
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response
    {
        return CatchResponse::success($this->vueEditorPagesModel->storeBy($request->post()));
    }
    
    /**
     *
     * @time 2022/03/13 17:38
     * @param $id
     * @return Response
     */
    public function read($id) : Response
    {
        return CatchResponse::success($this->vueEditorPagesModel->findBy($id));
    }
    
    /**
     *
     * @time 2022/03/13 17:38
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) : Response
    {
        return CatchResponse::success($this->vueEditorPagesModel->updateBy($id, $request->post()));
    }
    
    /**
     *
     * @time 2022/03/13 17:38
     * @param $id
     * @return Response
     */
    public function delete($id) : Response
    {
        return CatchResponse::success($this->vueEditorPagesModel->deleteBy($id));
    }

    /**
     * 切换页面
     * @param Request $request
     * @param $id
     */
    public function selectPage(Request $request) : \think\Response
    {
        $id = input('id');
        if ($id)
        {
            VueEditorPagesModel::where('user_id','=',$request->user()->id)->update(['edit_status'=>0]);
            VueEditorPagesModel::where('user_id','=',$request->user()->id)->where('id','=',$id)->update(['edit_status'=>1]);
            return CatchResponse::success($id);
        }
        return CatchResponse::fail('切换页面失败');
    }

    /**
     * 切换页面
     * @param Request $request
     * @param $id
     */
    public function newPage(Request $request) : \think\Response
    {
        $data = $request->post();
        $data['user_id'] = $request->user()->id;
        return CatchResponse::success($this->vueEditorPagesModel->storeBy($data));
    }
}