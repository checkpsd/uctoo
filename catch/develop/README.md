develop 模块是一个用于开发阶段设计数据结构、生成代码、设计页面、设计流程等开发工作的工具集模块。  

# 概述  

  本模块的设计目标是提供开发人员、产品人员等相关角色，可以快速用于开发管理后台和移动应用。  
  
## 主要特性  

### 一. codelabs 模块  
1. 支持通过可复用的模板，可视化配置和生成vue、vue-element等页面文件源码，开发者只需掌握vue知识，此外无任何额外学习成本。  
2. 支持通过指定数据源，快速获取用于页面显示的数据结构。（独立部署版支持，免费版不支持）  
3. 开发者可以使用codelabs更直观、快捷的开发和调试可复用模板、数据结构、算法，以及通过已有可复用预制构件快速生成可复用代码  
4. 提供了两个javascript语法分析工具，JsParser可以用来生成javascript源码的AST和Token树，js_merge()可以用来语法正确的合并两段javascript源码。 

### 二. vue editor 模块   
1. 支持可视化动态页面搭建的模块。采用`JSON Schema`描述页面结构、页面数据、配置数据等信息。前端独立项目开源地址 https://gitee.com/UCT/uctoo-vue-editor 。
2. UCToo线上运营版本地址 https://vueeditor.uctoo.com ，帐号与 www.uctoo.com 帐号相同。  

## 产品架构  
### Codelabs原理
  codelabs模块实现了一个典型的低代码、无代码运行逻辑框架。总结低代码、无代码产品，一般逻辑都是将模板、数据结构、算法进行分离，形成可自由排列组合的可复用预制构件，然后提供一个用户可视化配置界面，再将各种可复用预制构件有机组合起来，生成最终可运行的程序。
  codelabs先内置实现了一套UCToo headless CMS模块CURD页面可视化配置生成的模板、数据结构和算法的组合。开发者可以参考codelabs已实现的一套低代码示例，开发更多的可复用模板、数据结构和算法。通常用户可视化配置界面的初始化由模板、数据结构、算法的一个组合获得。

1. 可复用的页面模板文件位于 extend/catcher/command/stubs/elm 目录下。elmform.stub模板用于通过表结构生成默认的管理后台curd.vue页面。routerjs.stub模板用于生成模块router.js文件。  
2. 用于生成curd.vue页面和router.js的命令行工具位于extend/catcher/command/Tools/CreateElmformCommand.php文件。命令行需要先在config/console.php文件中注册才可以在框架中任意位置通过代码执行命令行工具（其实在代码中动态注册也可以）。
3. 页面生成命令行中采用了querylist（http://www.querylist.cc/）动态处理模板，querylist的DOM操作方法和API与jquery基本完全相同。
4. 页面生成命令行中合并控件javascript源码和模板中javascript源码，采用了https://github.com/mck89/peast 进行语法树分析。
5. codelabs将自定义模板/控件扩展开发复杂度分解控制在模板/控件级别。codelabs合并javascript源码虽然用到了语法树分析有一定复杂度，但是vue模板/javascript语法是有限集合，js_merge()函数完备后就不会再增加工作量，而自定义模板/控件开发是无限集合，会持续增加工作量的事情要尽可能简单。 
6. 理论上兼容vue component机制，component都可以采用codelabs的套路做成可视化配置component。

## 安装教程
1. 可手动复制develop模块前后端代码至本地开发环境项目目录，UCToo前后端模块代码目录。后台项目源码 gitee.com/uctoo/uctoo ,前端vue项目源码地址 https://gitee.com/UCT/uctoo-app-server-vue 
2. 管理员帐号登录管理后台安装develop模块。
3. 由于代码生成属于开发阶段的工具，可能会对运营环境产生不可预知的影响，因此不建议在运营环境开放代码生成功能提供用户使用。demo.uctoo.com 测试环境仅提供体验界面，无执行权限。

### 运行环境依赖
1. 使用页面生成功能需先安装 composer require jaeger/querylist 和 composer require mck89/peast，querylist用于解析html标签，peast用于解析javascript代码。
2. 建议PHP版本大于7.4
3. 生成的CURD页面源码依赖formOperate.js对应的版本，可从https://gitee.com/UCT/uctoo-app-server-vue 项目升级对应文件。
4. 由于加入了登录uctoo应用市场功能，因此依赖appstore模块。如果只使用codelabs的本地运行模式，不登录uctoo应用市场，也可以无需安装appstore模块。

## 使用手册
  免费SaaS版地址：admin.uctoo.com 注册帐号，登录控制台，develop/codelabs功能使用。  
  独立部署版体验地址：demo.uctoo.com ，使用手册请参考文档  [codelabs-user-manual.md](https://gitee.com/uctoo/uctoo/blob/master/catch/develop/doc/codelabs-user-manual.md)  
  模块使用界面截图：
  <table>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/codelabs.png"></td>
      </tr>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/elmform.png"></td>
      </tr>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/component_config.png"></td>
      </tr>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/jsparser.png"></td>
      </tr>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/jsmerge.png"></td>
      </tr>
  </table>
  
### 模版
1. 如果输入了模版代码则优先使用输入的模版代码进行代码生成。如果输入模版代码为空，则采用选择的模版文件进行代码生成。
2. elmform.stub模板用于生成headless CMS模块默认CURD界面，加入了版本管理，v1.0.0采用gitee企业版管理后台界面风格。

### 数据结构
1. 如果输入了数据结构则优先使用输入的数据结构进行代码生成（免费版必须人工输入数据结构）。如果输入数据结构为空，则采用数据源的配置进行代码生成。免费版codelabs不支持数据源自动获取数据结构功能（仍然需要配置数据源）。购买独立部署版即可使用数据源自动获取数据结构功能，配置从本地数据库表获取数据结构。
2. 如下数据结构是用于生成CURD页面的一个典型数据结构，可以通过修改以下数据结构的值，进行模板配置的初始化和生成最终代码。
```json
{
	"controller": {
		"module": "test",
		"controller": "catchAdmin\\test\\controller\\test",
		"table": "test",
		"model": "catchAdmin\\test\\model\\test",
		"restful": true
	},
	"table_fields": [{
		"field": "module",
		"type": "varchar",
		"length": 50,
		"nullable": false,
		"index": "",
		"default": "",
		"comment": "模块",
		"unsigned": false,
		"inputType": "input"
	}, {
		"field": "operate",
		"type": "varchar",
		"length": 20,
		"nullable": false,
		"index": "",
		"default": "",
		"comment": "操作",
		"unsigned": false,
		"inputType": "textarea"
	}, {
		"field": "route",
		"type": "varchar",
		"length": 100,
		"nullable": false,
		"index": "",
		"default": "",
		"comment": "路由",
		"unsigned": false,
		"inputType": "input"
	}],
	"table_extra": {
		"primary_key": "id",
		"created_at": true,
		"soft_delete": true,
		"creator_id": true,
		"engine": "InnoDB",
		"comment": "测试"
	},
	"create_controller": true,
	"create_model": true,
	"create_migration": true,
	"create_table": true,
	"create_crudform": true
}
```

### 算法
1. 暂不支持算法代码的在线运行。目前只支持通过选择的算法文件生成代码。

### 模板配置
1. 保存了模板和数据结构后，就可以通过模板配置->初始配置获取模板初始配置界面。可以在模板配置区域进行可视化界面配置。
2. 选择了字段对应的控件类型后，可以通过操作->配置按钮，对控件进行可视化配置，保存控件配置后，即已生成控件源码并保存在字段对应的component_code节点。
3. 保存模板配置的数据用于代码生成。

### 代码生成
1. 页面文件的正常运行和文件路径、文件名等数据相关，因此代码生成前需要配置输出设置。（免费版不支持生成代码保存至文件，但仍需设置）
2. 以上配置完成后，通过代码生成功能，即可获得生成的代码。生成的代码保存至输出设置对应的目录和文件名即可在前端框架中正常使用。（对应的后端接口需能正常访问）
3. test模块前端项目的curd.vue页面是一个通过代码生成的测试页面。

## 开发说明  

1. 默认生成的后台代码和前端代码主要用于对元数据的增删改查，属于无状态的接口，也主要提供管理员使用，不建议进行修改。有状态的接口和页面，可以参考自动生成的代码新增开发。
2. 用QueryList处理模板,会自动把自闭合标签都处理成闭合标签，例如<input />输出成<input></input>，对功能没影响。
3. 由于QueryList无法处理 @click、@close等@开头的标签事件，因此设计模板时所有@都用el-event-at_ 人工替换，动态生成页面后，再程序替换回 @。
4. 页面文件的name属性需要遵循 模块名_控制器名 （即与router.js中的前端路由定义一致），页面的keepalive状态才有效。
5. 本代码生成模块主要提供一种动态页面生成方法，不同模板对应的页面生成命令算法也不同，需要开发者自行根据不同的可复用模板编写动态生成算法。
6. 本开源版本代码生成模块目前仅使用了el-input组件（input、textarea）和select组件，开发者可以自行在页面生成命令中根据inputType生成其他可复用组件。
7. 合并控件javascript源码和模板javascript源码时，基本只能通过分析语法树的方式进行解决，为此本模块实现了一个js_merge()方法，目前仅针对特定结构javascript代码段。
8. 本模块附带提供了JsParser类和用户界面，用于分析javascript语法树，和测试js_merge()方法，在开发控件模版时很有用。
9. mck89/peast库只能解析符合ES规范的javascript源码，因此，模板中的占位符应在进行语法分析前替换成符合ES规范的源码块，或者采用符合ES语法规范的占位符，例如，el_js_replace_fields:''
            
### 模块roadmap
1. 实现更多可自动生成的控件类型，使生成的页面不仅只管理员可用，还可以提供用户使用。
2. 管理后台增加可复用模板库，开发者可共享模板。
3. 将代码编辑区从textarea控件更换为开源代码编辑器。
4. 支持控件设置数据源、设置验证规则等。
5. 支持在线算法运行。
6. 支持从开发者服务端获取模板文件、数据源、算法文件，以支持不开源发布的模板、算法文件。
7. 模板配置区域可根据模板文件、数据源、算法文件等前置输入自定义呈现形式。
8. 在UCToo应用市场提供模板交易板块和分成机制。  
9. 加入社交开发特性。
10. 所生成代码的在线执行。
11. 废弃命令行方式生成代码的方案，模板代码生成也统一成element模块中控件代码生成相同的微服务的方式。
12. 采用代码生成相同的方案，将模板配置和控件配置统一采用动态列table方式。
13. 数据源增加从数据表指定字段获取配置数据（即兼容控件配置从特定数据表字段获取渲染界面的数据）。
14. 扩展js_merge()方法成为通用合并javascript源码的方法。
15. 采用codelabs重构codelabs，即写一个codelabs的可复用模板，可视化配置生成codelabs源码，大致可用于证明codelabs这套无代码/低代码机制是图灵完备的。

## 扩展开发  
可以参考生成curd.vue的示例，进行更多可复用模板的扩展开发。建议的开发流程：  
1. 先根据需求或根据设计稿，写一个完整可用的vue页面。 
2. 分析出页面可复用的部分以及需要动态替换的内容，抽取出公共部分形成可复用模板文件(参考elmform.stub模板)，模板文件结构需符合[vue单文件组件规范](https://cn.vuejs.org/v2/guide/single-file-components.html)。   
3. 根据可复用模板文件，写一个动态生成页面的命令行工具（参考CreateElmformCommand.php）。
4. 在控制台执行命令行工具，或者在需要的代码处调用命令行工具，输入用于填充可复用模板的动态数据（一般是json数据），保存生成的页面源码。
5. 手动合并生成的页面源码，也可以在命令行设计时加入输出路径参数，直接将生成的页面源码路径设置为前端项目开发目录（可能要考虑加入检测默认覆盖已有文件的特性）。



  具体请参考开源版开发手册 https://www.kancloud.cn/doc_uctoo/uctoo_dev 及 本开源项目示例  