<?php

namespace catchAdmin\miniapp\model;

use catcher\base\CatchModel as Model;
/**
 *
 * @property int $id
 * @property int $user_id
 * @property string $openid
 * @property string $unionid
 * @property string $activity_id
 * @property int $expiration_time
 * @property int $valid
 * @property string $iv
 * @property string $encryptedData
 * @property string $shareTicket
 * @property string $member_count
 * @property string $room_limit
 * @property string $path
 * @property string $version_type
 * @property int $target_state
 * @property string $template_info
 * @property string $to_openid
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class WxaUpdatableMessage extends Model
{
    
    public $field = [
        //
        'id',
        // 小程序用户表id
        'user_id',
        // openid
        'openid',
        // unionid
        'unionid',
        // 动态消息的 ID
        'activity_id',
        // activity_id 的过期时间戳。默认24小时后过期。
        'expiration_time',
        // 验证是否通过
        'valid',
        // 加密算法的初始向量，详细见加密数据解密算法
        'iv',
        // 经过加密的activityId，解密后可得到原始的activityId
        'encryptedData',
        // shareTicket
        'shareTicket',
        // 状态 0 时有效，文字内容模板中 member_count 的值
        'member_count',
        // 状态 0 时有效，文字内容模板中 room_limit 的值
        'room_limit',
        // 状态 1 时有效，点击「进入」启动小程序时使用的路径。
        'path',
        // 状态1时有效，点击「进入」启动小程序时使用的版本。有效参数值为：develop（开发版），trial（体验版），release（正式版）
        'version_type',
        // 动态消息修改后的状态。0=未开始，1=已开始
        'target_state',
        // 动态消息对应的模板信息
        'template_info',
        // 接收人openid
        'to_openid',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    ];
    
    public $name = 'wxa_updatable_message';
}