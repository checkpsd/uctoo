<?php

namespace catchAdmin\miniapp\controller;

use catchAdmin\wechatopen\model\WechatopenMiniappUsers;
use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\miniapp\model\WxaUpdatableMessage as WxaUpdatableMessageModel;
use catcher\Utils;
use think\facade\Log;
use think\Response;
use uctoo\uctoocloud\client\Http;

class WxaUpdatableMessage extends CatchController
{
    
    protected $wxaUpdatableMessageModel;
    
    /**
     *
     * @time 2022/03/15 15:16
     * @param WxaUpdatableMessageModel $wxaUpdatableMessageModel
     * @return mixed
     */
    public function __construct(WxaUpdatableMessageModel $wxaUpdatableMessageModel)
    {
        $this->wxaUpdatableMessageModel = $wxaUpdatableMessageModel;
    }
    
    /**
     *
     * @time 2022/03/15 15:16
     * @return Response
     */
    public function index() : Response
    {
        return CatchResponse::paginate($this->wxaUpdatableMessageModel->getList());
    }
    
    /**
     *
     * @time 2022/03/15 15:16
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response
    {
        return CatchResponse::success($this->wxaUpdatableMessageModel->storeBy($request->post()));
    }
    
    /**
     *
     * @time 2022/03/15 15:16
     * @param $id
     * @return Response
     */
    public function read($id) : Response
    {
        return CatchResponse::success($this->wxaUpdatableMessageModel->findBy($id));
    }
    
    /**
     *
     * @time 2022/03/15 15:16
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) : Response
    {
        return CatchResponse::success($this->wxaUpdatableMessageModel->updateBy($id, $request->post()));
    }
    
    /**
     *
     * @time 2022/03/15 15:16
     * @param $id
     * @return Response
     */
    public function delete($id) : Response
    {
        return CatchResponse::success($this->wxaUpdatableMessageModel->deleteBy($id));
    }

    /**
     *
     * @time 2022/03/15 15:16
     * @param Request $request
     * @return Response
     */
    public function addMessage(\think\Request $request) : Response
    {
        $data = $request->post();
        $token = $request->header('token');
        $user = WechatopenMiniappUsers::where('token',$token)->find();
        $host = trim(Utils::config('wechatopen.host'));//获取管理后台配置的微信第三方平台地址 http://serv.uctoo.com
        $appid = input('appid') ? input('appid') : $user->appid;
        $response = Http:: withHost($host)   //指定第三方平台
        ->withVerify(false)    //无需SSL验证
        ->product('/api/wechatopen')    //如果指定产品 则必填参数
        ->post('/message/wxopen/activityid/create', ['appid' => $appid]);  //当前小程序的appid
        $res = json_decode($response->body(),true);
        trace($res,'debug');
        if($res['code'] == 1){ //实测成功的code是1 与微信官方文档不一致

        }
        Log::write('addMessage', 'debug');
        Log::write($res, 'debug');
        $data['user_id'] = $user->id;
        $data['openid'] = $user->openid;
        $data['activity_id'] = $res['data']['activity_id'];
        $data['expiration_time'] = $res['data']['expiration_time'];
        $msgId = $this->wxaUpdatableMessageModel->storeBy($data);
        $data['id'] = $msgId;
        return CatchResponse::success($data);
    }

    /**
     *
     * @time 2022/03/15 15:16
     * @param Request $request
     * @return Response
     */
    public function getMessage(\think\Request $request) : Response
    {
        return CatchResponse::success($this->wxaUpdatableMessageModel->findBy(input('id')));
    }

    /**
     *
     * @time 2022/03/15 15:16
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateMessage(\think\Request $request) : Response
    {
        return CatchResponse::success($this->wxaUpdatableMessageModel->updateBy(input('id'), $request->post()));
    }
}