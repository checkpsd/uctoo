<?php

namespace catchAdmin\miniapp\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\miniapp\model\WechatCloud as wechatCloudModel;
use catchAdmin\wechatopen\model\Applet;
use catchAdmin\wechatopen\model\AdminApplet;
use EasyWeChat\OpenPlatform\Application;
use think\helper\Str;
use uctoo\ThinkEasyWeChat\OpenPlatform\Cloud\CloudEnv\ServiceProvider;

class BatchCloud extends CatchController
{
    protected $wechatCloudModel;
    
    public function __construct(WechatCloudModel $wechatCloudModel)
    {
        $this->wechatCloudModel = $wechatCloudModel;
    }

    /**
     * 引入微信控制器的traits
     */
    use \uctoo\library\traits\Wechatopen;
    
    /**
     * 列表
     * @time 2021年03月23日 19:48
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->wechatCloudModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年03月23日 19:48
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->wechatCloudModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年03月23日 19:48
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->wechatCloudModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年03月23日 19:48
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->wechatCloudModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年03月23日 19:48
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->wechatCloudModel->deleteBy($id));
    }

    /**
     * 创建环境  //TODO:微信交互接口重构到app/uctoo目录
     * @param Request $request
     * @param Application $app
     * @param string $alias 环境别名，要以a-z开头，不能包含 a-zA-z0-9- 以外的字符
     * @return  {
    "code": 10000,
    "message": "批量创建云开发环境成功",
    "data": {
    "errcode": 0,
    "errmsg": "ok",
    "tranid": "20210326630002481462961",
    "env": "weiyohotest-0gfa1ckv386bfde1"
    }
    }
     */
    public function createenv(Request $request,Application $app) : \think\Response
    {
            $alias = $request->post('alias');
            $cloud_env = $app->register(new ServiceProvider)->cloud_env;
            $res = $cloud_env->createenv($alias); //TODO:创建的云环境保存数据库
            if($res){
                return CatchResponse::success($res,'批量创建云开发环境成功');
            }else{
                return CatchResponse::fail('请管理员先选中要操作的小程序');
            }
    }

    /**
     * 使用腾讯云环境
     * @param Request $request
     * @param Application $app
     */
    public function modifyenv(Request $request,Application $app,string $env) : \think\Response
    {
        $cloud_env = $app->register(new ServiceProvider)->cloud_env;

        $res = $cloud_env->modifyenv($env);
        if($res){
            return CatchResponse::success($res,'使用腾讯云环境成功');
        }else{
            return CatchResponse::fail('使用腾讯云环境失败');
        }
    }

    /**
     * 获取环境列表
     * @param Request $request
     * @param Application $app
     * @trturn  {
    "code": 10000,
    "message": "获取环境列表成功",
    "data": {
    "errcode": 0,
    "errmsg": "ok",
    "info_list": [
    {
    "env": "weiyohotest-0gfa1ckv386bfde1",
    "alias": "weiyohotest",
    "create_time": "2021-03-26 16:04:58",
    "update_time": "2021-03-26 16:05:47",
    "status": "NORMAL",
    "package_id": "",
    "package_name": ""
    }
    ]
    }
    }
     */
    public function describeenvs(Request $request,Application $app) : \think\Response
    {
        $cloud_env = $app->register(new ServiceProvider)->cloud_env;
        $res = $cloud_env->describeenvs();
        if($res['errcode']){  //
            return CatchResponse::success($res,'获取环境列表成功');
        }else{
            return CatchResponse::fail('获取环境列表失败');
        }
    }

    /**
     * 批量绑定小程序和环境
     * @param Request $request
     * @param Application $app
     * @postbody TODO：多env 数组绑定
     */
    public function batchshareenv(Request $request,Application $app) : \think\Response
    {       //查询管理员当前选中的applet
        $env = $request->post('env');
        $appids = explode(',',$request->post('appids'));
        $data = [["env"=>$env,"appids"=>$appids]];
        $cloud_env = $app->register(new ServiceProvider)->cloud_env;
        $res = $cloud_env->batchshareenv($data);//
        if(!isset($res['err_list'])){  //
            return CatchResponse::success($res,'批量绑定小程序和环境成功');
        }else{
            return CatchResponse::fail('批量绑定小程序和环境失败');
        }
    }

    /**
     * 批量查询小程序绑定的环境id
     * @param Request $request
     * @param Application $app
     * @return  {
    "code": 10000,
    "message": "批量查询小程序绑定的环境id成功",
    "data": {
    "errcode": 0,
    "errmsg": "ok",
    "relation_data": [
    {
    "appid": "wx617e58c9f719235d",
    "env_list": [
    "weiyohotest-0gfa1ckv386bfde1"
    ]
    }
    ],
    "err_list": []
    }
    }
     */
    public function batchgetenvid(Request $request,Application $app) : \think\Response
    {
        $appids = explode(',',$request->post('appids'));
        $cloud_env = $app->register(new ServiceProvider)->cloud_env;
        $res = $cloud_env->batchgetenvid($appids);
        if($res){
            return CatchResponse::success($res,'批量查询小程序绑定的环境id成功');

        }else{
            return CatchResponse::fail('请管理员先选中要操作的小程序');
        }
    }
}