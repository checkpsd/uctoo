<?php
namespace catchAdmin\appstore\model\search;

trait AppstoreSearch
{
    public function searchTitleAttr($query, $value, $data)
    {
        return $query->whereLike('title', $value);
    }

    public function searchModelAttr($query, $value, $data)
    {
        return $query->whereLike('model', $value);
    }

    public function searchStatusAttr($query, $value, $data)
    {
        return $query->where($this->aliasField('status'), $value);
    }

}
