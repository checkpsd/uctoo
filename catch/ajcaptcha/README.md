
# 概述  

  ajcaptcha 模块是一个行为验证码模块。集成自开源项目 https://gitee.com/anji-plus/captcha 
  
## 主要特性  

### 
1. 支持多端兼容的滑动拼图、文字点选两种行为验证码方式。
2. 已实现PC端管理后台登录场景采用ajcaptcha验证码示例。

## 产品架构  
### AJ-Captcha  
  详情请参考开源项目 https://gitee.com/anji-plus/captcha 
 
## 安装教程
1. 可手动复制ajcaptcha模块前后端代码至本地开发环境项目目录，UCToo前后端模块代码目录。后台项目源码 gitee.com/uctoo/uctoo ,前端vue项目源码地址 https://gitee.com/UCT/uctoo-app-server-vue 
2. 管理员帐号登录管理后台安装ajcaptcha模块。
3. 如果后端框架版本采用的是 gitee.com/uctoo/uctoo 主线版本，则已安装了相关依赖可直接使用。 如框架未安装ajcaptcha依赖库，需先在后台项目根目录运行命令行 `composer require fastknife/ajcaptcha dev-master` 安装依赖。
4. 如果管理后台vue项目采用的是 https://gitee.com/UCT/uctoo-app-server-vue 主线版本，则已安装了相关依赖可直接使用。 如前端框架未安装ajcaptcha依赖库，需先在前端项目根目录运行命令行 `npm install axios  crypto-js   -S`安装依赖。并将 src\components\verifition 目录和 src\assets\image 目录复制到对应位置。更新登录页面 src\views\login\index.vue 添加验证码。
5. 如果服务端运行环境采用的是 https://gitee.com/UCT/uctoo-docker ，则已经默认安装了openssl、php-gd相关运行库。 如运行环境未安装openssl、php-gd扩展，请先安装和打开扩展。

## 开发说明  

1. 本模块目前仅实现了 /captcha/get 、 /captcha/check 、 /captcha/verification 三个验证码相关服务接口， /captcha/verification 二次验证目前暂未用到。需要进行二次验证的开发者可根据自身具体需求参考AJ-Captcha开源项目自行扩展。

## 问题反馈
  开发者交流QQ群984748053
  更多信息请关注UCToo微信第三方运营平台 https://www.uctoo.com   
  
## 版权信息  
UCToo遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2014-2021 by UCToo (https://www.uctoo.com)

All rights reserved  