<?php

namespace catchAdmin\element\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\element\model\ElementComponents as ElementComponentsModel;
use think\facade\Log;
use think\Response;
use think\facade\View;
use catchAdmin\develop\model\CodelabsTemplates;

class ElementComponents extends CatchController
{
    
    protected $elementComponentsModel;
    
    /**
     *
     * @time 2021/11/23 16:34
     * @param ElementComponentsModel $elementComponentsModel
     * @return mixed
     */
    public function __construct(ElementComponentsModel $elementComponentsModel)
    {
        $this->elementComponentsModel = $elementComponentsModel;
    }
    
    /**
     *
     * @time 2021/11/23 16:34
     * @return Response
     */
    public function index() : Response
    {
        return CatchResponse::paginate($this->elementComponentsModel->getList());
    }
    
    /**
     *
     * @time 2021/11/23 16:34
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response
    {
        return CatchResponse::success($this->elementComponentsModel->storeBy($request->post()));
    }
    
    /**
     *
     * @time 2021/11/23 16:34
     * @param $id
     * @return Response
     */
    public function read($id) : Response
    {
        return CatchResponse::success($this->elementComponentsModel->findBy($id));
    }
    
    /**
     *
     * @time 2021/11/23 16:34
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) : Response
    {
        return CatchResponse::success($this->elementComponentsModel->updateBy($id, $request->post()));
    }
    
    /**
     *
     * @time 2021/11/23 16:34
     * @param $id
     * @return Response
     */
    public function delete($id) : Response
    {
        return CatchResponse::success($this->elementComponentsModel->deleteBy($id));
    }

    /**
     * 初始化组件配置项
     * @time 2021/11/23 16:34
     * @return Response
     */
    public function initconfig() : Response
    {
        $codeId = input('id');
        $data = input('data');
        $components = $this->elementComponentsModel->where('name',$data['inputType'])->find();
        $tag = $components->name;
        $template = CodelabsTemplates::where('name',$data['inputType'])->find();          //目前仅使用了codelabs里的可视化自定义配置项，暂未使用element_components里的specification配置全量
        $config_json = json_decode($template->config_json,true);
        $config_data = [];
        foreach ($config_json as $k => $v) {  //只将需要用户可视化配置的项返回前端，其他自动计算的配置项后台处理
            if($v['uiSchema']['display'] == 'block'){
                $config_data[$k] = $v;
            }
        }
        //todo:自动从桩模板读取可配置项，并与数据库中设置的配置项比对，如果不一致则报错，或者默认以桩模板或数据库中的配置项为准

        return CatchResponse::success(json_encode($config_data,JSON_UNESCAPED_UNICODE));
    }

    /**
     * 接收input控件配置数据，结合控件桩模板生成控件代码
     * @time 2021/11/23 16:34
     */
    public function input()
    {
        $config_json = json_decode(input('config_json'),true);
        $configRow = input('configRow');
        $preview = input('preview');
        foreach ($config_json as $k => $v) {
            $config[$v['key']] = $v['value'];
        }
        //自动计算无需用户配置的项
        $config['v_model'] = 'formFieldsData.'.$configRow['field'] ;
        // 模板变量赋值
        View::assign('v_model',$config['v_model']? $config['v_model']:'formFieldsData.');                     //计算值
        View::assign('placeholder',$config['placeholder']? $config['placeholder']:'请输入');                  //配置值
        View::assign('type',$config['type']? $config['type']:'text');                       //配置值
        // 模板输出
        $html =  View::fetch('../catch/element/view/input.vue');
        if($preview){
            return CatchResponse::success($html);
        }
        //todo:将控件配置结果入库保存，再次打开已配置过的控件时，加载已配置过的数据
        return CatchResponse::success($html);
    }

    /**
     * 接收控件配置数据，结合控件桩模板生成控件代码
     * @time 2021/11/23 16:34
     */
    public function select()
    {
        $config_json = json_decode(input('config_json'),true);
        $configRow = input('configRow');
        $preview = input('preview');
        foreach ($config_json as $k => $v) {
            $config[$v['key']] = $v['value'];
        }
        //自动计算无需用户配置的项
        $config['v_model'] = 'formFieldsData.'.$configRow['field'] ;
        $config['remote_method'] = 'query'.$configRow['field'].'OptionList';
        $config['optionList'] = $configRow['field'].'OptionList';
        // 模板变量赋值
        View::assign('v_model',$config['v_model']? $config['v_model']:'formFieldsData.');                     //计算值
        View::assign('placeholder',$config['placeholder']? $config['placeholder']:'请选择');                  //配置值
        View::assign('remote_method',$config['remote_method']? $config['remote_method']:'queryOptionList');   //计算值
        View::assign('optionList',$config['optionList']? $config['optionList']:'optionList');                 //计算值
        View::assign('key',$config['key']? $config['key']:'name');                                            //配置值
        View::assign('label',$config['label']? $config['label']:'name');                                      //配置值
        View::assign('value',$config['value']? $config['value']:'name');                                      //配置值
        View::assign('tables',$config['tables']? $config['tables']:'tables');                                 //配置值
        View::assign('field',$config['field']? $config['field']:'field');                                     //配置值
        // 模板输出
        $html =  View::fetch('../catch/element/view/select.vue');
        if($preview){
            return CatchResponse::success($html);
        }
        return CatchResponse::success($html);
    }

    /**
     * 接收控件配置数据，结合控件桩模板生成控件代码
     * @time 2021/11/23 16:34
     */
    public function upload()
    {
        $config_json = json_decode(input('config_json'),true);
        $configRow = input('configRow');
        $preview = input('preview');
        foreach ($config_json as $k => $v) {
            $config[$v['key']] = $v['value'];
        }
        //自动计算无需用户配置的项
        $config['Field'] = $configRow['field'];
        $config['handleFieldExceed'] = 'handle'.$configRow['field'].'Exceed';
        $config['handleFieldSuccess'] = 'handle'.$configRow['field'].'Success';
        $config['handleFieldRemove'] = 'handle'.$configRow['field'].'Remove';
        $config['beforeFieldUpload'] = 'before'.$configRow['field'].'Upload';
        $config['submitFieldUpload'] = 'submit'.$configRow['field'].'Upload';

        // 模板变量赋值
        View::assign('Field',$config['Field']? $config['Field']:'Field');                   //计算值
        View::assign('handleFieldExceed',$config['handleFieldExceed']? $config['handleFieldExceed']:'handleFieldExceed');                   //计算值
        View::assign('handleFieldSuccess',$config['handleFieldSuccess']? $config['handleFieldSuccess']:'handleFieldSuccess');   //计算值
        View::assign('handleFieldRemove',$config['handleFieldRemove']? $config['handleFieldRemove']:'handleFieldRemove');                 //计算值
        View::assign('beforeFieldUpload',$config['beforeFieldUpload']? $config['beforeFieldUpload']:'beforeFieldUpload');                 //计算值
        View::assign('submitFieldUpload',$config['submitFieldUpload']? $config['submitFieldUpload']:'submitFieldUpload');                 //计算值

        View::assign('filetype',$config['filetype']? $config['filetype']:'image');                                                     //配置值
        View::assign('count',$config['count']? $config['count']:'1');                                                                  //配置值
        View::assign('uploadtip',$config['uploadtip']? $config['uploadtip']:'只能上传jpg/png文件，且不超过5MB');                                      //配置值
        // 模板输出
        $html =  View::fetch('../catch/element/view/upload.vue');
        if($preview){
            return CatchResponse::success($html);
        }
        return CatchResponse::success($html);
    }

    /**
     * 接收控件配置数据，结合控件桩模板生成控件代码
     * @time 2021/11/23 16:34
     */
    public function datepicker()
    {
        $config_json = json_decode(input('config_json'),true);
        $configRow = input('configRow');
        $preview = input('preview');
        foreach ($config_json as $k => $v) {
            $config[$v['key']] = $v['value'];
        }
        //自动计算无需用户配置的项
        $config['field'] = $configRow['field'];
        // 模板变量赋值
        View::assign('field',$config['field']? $config['field']:'formFieldsData.');                     //计算值
        View::assign('placeholder',$config['placeholder']? $config['placeholder']:'选择日期');                  //配置值
        View::assign('type',$config['type']? $config['type']:'date');                       //配置值
        // 模板输出
        $html =  View::fetch('../catch/element/view/datepicker.vue');
        if($preview){
            return CatchResponse::success($html);
        }
        //todo:将控件配置结果入库保存，再次打开已配置过的控件时，加载已配置过的数据
        return CatchResponse::success($html);
    }

    /**
     * 接收控件配置数据，结合控件桩模板生成控件代码
     * @time 2021/11/23 16:34
     */
    public function switch()
    {
        $config_json = json_decode(input('config_json'),true);
        $configRow = input('configRow');
        $preview = input('preview');
        foreach ($config_json as $k => $v) {
            $config[$v['key']] = $v['value'];
        }
        //自动计算无需用户配置的项
        $config['field'] = $configRow['field'];
        // 模板变量赋值
        View::assign('field',$config['field']? $config['field']:'formFieldsData.');                     //计算值
        View::assign('active_text',$config['active_text']? $config['active_text']:'开');                  //配置值
        View::assign('inactive_text',$config['inactive_text']? $config['inactive_text']:'关');                  //配置值
        View::assign('active_value',$config['active_value']? $config['active_value']:'1');                  //配置值
        View::assign('inactive_value',$config['inactive_value']? $config['inactive_value']:'0');                  //配置值
        // 模板输出
        $html =  View::fetch('../catch/element/view/switch.vue');
        if($preview){
            return CatchResponse::success($html);
        }
        //todo:将控件配置结果入库保存，再次打开已配置过的控件时，加载已配置过的数据
        return CatchResponse::success($html);
    }

    /**
     * 接收控件配置数据，结合控件桩模板生成控件代码
     * @time 2021/11/23 16:34
     */
    public function chinaareadata()
    {
        $config_json = json_decode(input('config_json'),true);
        $configRow = input('configRow');
        $preview = input('preview');
        foreach ($config_json as $k => $v) {
            $config[$v['key']] = $v['value'];
        }
        //自动计算无需用户配置的项
        $config['field'] = $configRow['field'];
        // 模板变量赋值
        View::assign('field',$config['field']? $config['field']:'formFieldsData.');                     //计算值
        // 模板输出
        $html =  View::fetch('../catch/element/view/chinaareadata.vue');
        if($preview){
            return CatchResponse::success($html);
        }
        //todo:将控件配置结果入库保存，再次打开已配置过的控件时，加载已配置过的数据
        return CatchResponse::success($html);
    }
}