<?php

namespace catchAdmin\element\model;

use catcher\base\CatchModel as Model;
/**
 *
 * @property int $id
 * @property string $name
 * @property string $tag
 * @property string $docs_url
 * @property string $specification
 * @property string $version
 * @property string $remarks
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class ElementComponents extends Model
{
    
    public $field = [
        //
        'id',
        // 组件名，与https://github.com/ElemeFE/element/blob/master/components.json一致
        'name',
        // 组件标签
        'tag',
        // 组件文档url地址
        'docs_url',
        // 组件规范json，包含组件Attributes的全量信息
        'specification',
        // 版本
        'version',
        // 备注
        'remarks',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    ];
    
    public $name = 'element_components';
}