NoTee has an awesome debug feature. If enabled, an additional attribute is added to all generated html nodes. This
attribute contains the source file and line, the html node is coming from:

`<a href="" data-source="/var/www/template/index.html.php:99">Link</a>`

To enable debug mode, you need to add an additional flag when creating a new Instance of `NoTee\NodeFactory`:

    <?php
    
    require 'vendor/autoload.php';
    
    use NoTee\NodeFactory;
    use NoTee\DefaultEscaper;
    use NoTee\UriValidator;
    
    $nf = new NodeFactory(
         new DefaultEscaper('utf-8'),
         new UriValidator(),
         new BlockManager(),
         true, // this tells the NodeFactory to use debug mode
     );
     
     // now all created nodes contain the additional debug attribute