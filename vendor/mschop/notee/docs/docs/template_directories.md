NoTee supports a lot of very powerful features, common template engines support. You can use the
template functionality, if you want to organise your view in several dependent dependencies. This can be useful for
multi-tenant setups or plugin api's.

Imagine the following directory structure:

```
- project
    - plugins
        - pluginx
            - views
                -index.html.php
    - views
        - base
            - index.html.php
        - page
            - index.html.php
```

This plugin structure could be realized with NoTee with the following setup:

```
<?php

require 'vendor/autoload.php';

namespace YourNamespace;

use NoTee\NodeFactory;
use NoTee\DefaultEscaper;
use NoTee\UriValidator;
use NoTee\BlockManager;
use NoTee\Template;
    
global $noTee;
$noTee = new NodeFactory(
     new DefaultEscaper('utf-8'),
     new UriValidator(),
     new BlockManager()
 );
 require 'vendor/mschop/notee/global.php';
 
 $directories = [
    'views/base',
    'plugins/pluginx/views',
    'views/page
 ];
 $template = new Template($directories, $noTee);
 $noTee->setTemplate($template);
```

Now you can simply call the render function this way:

```
$template->render('index.html.php');
```

What now happens is the following. NoTee tries to include `views/page/index.html.php`. If it exists and returns
something, it stops further processing. If a template file returns something it does not extend the parents
implementation but replaces it. So this means, there is nothing more to do. If this file does not return something, it
tries to include the file `plugins/pluginx/views/index.html.php`. This process goes on, until some include returns
something.

I highly recommend to take a look at the NoTee test `tests/ExtensibilityTest.php`. This test uses an example project to
test the behaviour of NoTee. It also demonstrates, how this functionality works.