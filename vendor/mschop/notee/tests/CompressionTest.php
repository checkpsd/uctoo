<?php


namespace NoTee;


use PHPUnit\Framework\TestCase;

class CompressionTest extends TestCase
{
    private NodeFactory $nf;

    /**
     * @before
     */
    public function before()
    {
        $this->nf = new NodeFactory(new DefaultEscaper('utf-8'), new UriValidator(), new BlockManager());
    }

    public function testLeaveOutClosingLi()
    {
        $this->assertEquals('<ul><li>wert1<li>wert2</ul>', (string)$this->nf->ul(
            $this->nf->li('wert1'),
            $this->nf->li('wert2'),
        ));
    }
}