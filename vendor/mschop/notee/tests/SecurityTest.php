<?php

declare(strict_types=1);

namespace NoTee;

use InvalidArgumentException;
use NoTee\Nodes\DefaultNode;
use PHPUnit\Framework\TestCase;

class SecurityTest extends TestCase
{
    private NodeFactory $nf;

    /**
     * @before
     */
    public function before()
    {
        $this->nf = new NodeFactory(new DefaultEscaper('utf-8'), new UriValidator(), new BlockManager());
    }

    private function getEscaper()
    {
        return new DefaultEscaper('utf-8');
    }

    public function test_EscapesAttributes()
    {
        $node = new DefaultNode('div', $this->getEscaper(), ['class' => '"classname']);
        $this->assertEquals('<div class="&quot;classname"></div>', (string)$node);
    }

    public function test_InvalidAttributeName_ThrowsException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->nf->a(['_invalid>attribute<name' => 'google.de']);
    }

    public function test_UriInjection()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->nf->a(
            ['href' => 'javascript: alert(1)']
        );
    }

    public function test_UriInjectionWithWhitespace()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->nf->a(['href' => '  javascript: alert(1)   ']);
    }

    public function test_EmptyUri()
    {
        $this->nf->a(['href' => '']);
        $this->assertTrue(true);
    }

    public function test_uriWithoutScheme()
    {
        $this->nf->a(['href' => 'www.google.com']);
        $this->assertTrue(true);
    }
}
