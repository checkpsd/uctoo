<?php

_extend('body', fn($parent) => _wrapper(
    $parent(),
    _text(' World'),
));

_extend('title', fn() => _text('NoTee' . $titleExtension));

return _include('base.html.php');