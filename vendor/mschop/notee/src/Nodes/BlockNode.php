<?php


namespace NoTee\Nodes;


use NoTee\BlockManagerInterface;
use NoTee\NodeInterface;

class BlockNode implements NodeInterface
{
    protected BlockManagerInterface $blockManager;
    protected string $name;

    public function __construct(BlockManagerInterface $blockManager, string $name)
    {
        $this->blockManager = $blockManager;
        $this->name = $name;
    }

    public function __toString(): string
    {
        return (string)$this->blockManager->compose($this->name);
    }
}