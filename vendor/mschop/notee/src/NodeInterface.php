<?php

namespace NoTee;


interface NodeInterface
{
    public function __toString() : string;
}
