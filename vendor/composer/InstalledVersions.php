<?php

namespace Composer;

use Composer\Semver\VersionParser;






class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '2b030edb571144ea36202fb75ce9ff3ee588c74c',
    'name' => 'jaguarjack/catchadmin',
  ),
  'versions' => 
  array (
    'adbario/php-dot-notation' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eee4fc81296531e6aafba4c2bbccfc5adab1676e',
    ),
    'alibabacloud/aas' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/actiontrail' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/adb' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/aegis' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/afs' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/airec' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/alidns' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/alikafka' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/alimt' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/aliprobe' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/aliyuncvc' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/appmallsservice' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/arms' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/arms4finance' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/baas' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/batchcompute' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/bss' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/bssopenapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cas' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cbn' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ccc' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ccs' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cdn' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cds' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cf' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/chatbot' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/client' => 
    array (
      'pretty_version' => '1.5.31',
      'version' => '1.5.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '19224d92fe27ab8ef501d77d4891e7660bc023c1',
    ),
    'alibabacloud/cloudapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cloudauth' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cloudesl' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cloudmarketing' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cloudphoto' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cloudwf' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cms' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/commondriver' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/companyreg' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cr' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/crm' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cs' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/csb' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/cusanalyticsconline' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dataworkspublic' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dbs' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dcdn' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dds' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/democenter' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dm' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dmsenterprise' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/domain' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/domainintl' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/drcloud' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/drds' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dts' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dybaseapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dyplsapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dypnsapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dysmsapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/dyvmsapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/eci' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ecs' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ecsinc' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/edas' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ehpc' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/elasticsearch' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/emr' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ess' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/facebody' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/fnf' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/foas' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ft' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/goodstech' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/gpdb' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/green' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/hbase' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/hiknoengine' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/hpc' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/hsm' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/httpdns' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/idst' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/imageaudit' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/imageenhan' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/imagerecog' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/imagesearch' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/imageseg' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/imm' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/industrybrain' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/iot' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/iqa' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/itaas' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ivision' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ivpd' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/jaq' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/jarvis' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/jarvispublic' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/kms' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/linkedmall' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/linkface' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/linkwan' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/live' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/lubancloud' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/lubanruler' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/market' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/mopen' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/mpserverless' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/mts' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/multimediaai' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/nas' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/netana' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/nlp' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/nlpautoml' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/nlscloudmeta' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/nlsfiletrans' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/objectdet' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ocr' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ocs' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/oms' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ons' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/onsmqtt' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/oos' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/openanalytics' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ossadmin' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ots' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/outboundbot' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/petadata' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/polardb' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/productcatalog' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/pts' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/push' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/pvtz' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/qualitycheck' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ram' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/rds' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/reid' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/retailcloud' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/rkvstore' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ros' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/rtc' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/saf' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/sas' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/sasapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/scdn' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/schedulerx2' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/sdk' => 
    array (
      'pretty_version' => '1.8.1256',
      'version' => '1.8.1256.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56ce8ac34c3e911cd431e3a00a5b68fe03af638',
    ),
    'alibabacloud/skyeye' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/slb' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/smartag' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/smc' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/sms' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/smsintl' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/snsuapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/sts' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/taginner' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/tesladam' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/teslamaxcompute' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/teslastream' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ubsms' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/ubsmsinner' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/uis' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/unimkt' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/visionai' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/vod' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/voicenavigator' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/vpc' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/vs' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/wafopenapi' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/welfareinner' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/xspace' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/xtrace' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/yqbridge' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'alibabacloud/yundun' => 
    array (
      'replaced' => 
      array (
        0 => '1.8.1256',
      ),
    ),
    'cache/adapter-common' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8788309be72aa7be69b88cdc0687549c74a7d479',
    ),
    'cache/filesystem-adapter' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f1faaae40aaa696ef899cef6f6888aedb90b419b',
    ),
    'cache/tag-interop' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b062b1d735357da50edf8387f7a8696f3027d328',
    ),
    'clagiordano/weblibs-configmanager' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c8ebcc62782313b1278afe802b120d18c07a059',
    ),
    'danielstjules/stringy' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df24ab62d2d8213bbbe88cc36fc35a4503b4bd7e',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.13.2',
      'version' => '1.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b668aef16090008790395c02c893b1ba13f7e08',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.12.1',
      'version' => '1.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4cf401d14df219fa6f38b671f5493449151c9ad8',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => 'v2.9.0',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '21fdabe2fc01e004e1966f200d900554876bc63c',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9c50f840f257bbb941e6f4a0e94ccf5db5c3f76c',
    ),
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a8c6e56ab3ffcc538d05e8155bb42269abf1a0c',
    ),
    'easywechat-composer/easywechat-composer' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3fc6a7ab6d3853c0f4e2922539b56cc37ef361cd',
    ),
    'erusev/parsedown' => 
    array (
      'pretty_version' => '1.7.4',
      'version' => '1.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb17b6477dfff935958ba01325f2e8a2bfa6dab3',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.14.0',
      'version' => '4.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ab42bd6e742c70c0a52f7b82477fcd44e64b75',
    ),
    'fastknife/ajcaptcha' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '21e239527b9d8e6f62a42664f2cb4bb2127d9064',
    ),
    'godruoyi/php-snowflake' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd8cbe72ed375b45033b7042e3d03340ce4fa479f',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.4.1',
      'version' => '7.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee0a041b1760e6a53d2a39c8c34115adc2af2c79',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '089edd38f5b8abba6cb01567c2a8aaa47cec4c72',
    ),
    'guzzlehttp/uri-template' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b945d74a55a25a949158444f09ec0d3c120d69e2',
    ),
    'intervention/image' => 
    array (
      'pretty_version' => '2.7.1',
      'version' => '2.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '744ebba495319501b873a4e48787759c72e3fb8c',
    ),
    'jaeger/g-http' => 
    array (
      'pretty_version' => 'V1.7.2',
      'version' => '1.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '82585ddd5e2c6651e37ab1d8166efcdbb6b293d4',
    ),
    'jaeger/phpquery-single' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fb80b4a0e5a337438d26e061ec3fb725c9f2a116',
    ),
    'jaeger/querylist' => 
    array (
      'pretty_version' => 'V4.2.8',
      'version' => '4.2.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '39dc0ca9c668bec7a793e20472ccd7d26ef89ea4',
    ),
    'jaguarjack/catchadmin' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '2b030edb571144ea36202fb75ce9ff3ee588c74c',
    ),
    'jaguarjack/file-generate' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '30030905a71a6761b3141a61fe97e484072f8163',
    ),
    'jaguarjack/migration-generator' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '9904314935376b9cd2735706f1d9009829372538',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8866a58fa866f6872f2a6ea0e04ad56480f0f440',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '094defdb4a7001845300334e7c1ee2335925ef99',
    ),
    'league/flysystem-cached-adapter' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd1925efb2207ac4be3ad0c40b8277175f99ffaff',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aa70e813a6ad3d1558fc927863d47309b4c23e69',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab8bc271e404909db09ff2d5ffa1e538085c0f22',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c66aefcafb4f6c269510e9ac46b82619a904c576',
    ),
    'mck89/peast' => 
    array (
      'pretty_version' => 'v1.13.11',
      'version' => '1.13.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '78c57966f3da5f223636ea0417d71ac6ff61e47f',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.3.5',
      'version' => '2.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fd4380d6fc37626e2f799f29d91195040137eba9',
    ),
    'mschop/notee' => 
    array (
      'pretty_version' => 'v1.0.0-beta',
      'version' => '1.0.0.0-beta',
      'aliases' => 
      array (
      ),
      'reference' => 'dae4bebc9d2d129c5328140d49a271dcddc89d2b',
    ),
    'mtdowling/cron-expression' => 
    array (
      'replaced' => 
      array (
        0 => '^1.0',
      ),
    ),
    'mtdowling/jmespath.php' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.8.3',
      'version' => '1.8.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b942d263c641ddb5190929ff840c68f78713e937',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.56.0',
      'version' => '2.56.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '626ec8cbb724cd3c3400c3ed8f730545b744e3f4',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.13.2',
      'version' => '4.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '210577fe3cf7badcc5814d99455df46564f3c077',
    ),
    'overtrue/easy-sms' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'daa0b4308ec0e3c112888c288d14d473be6aabee',
    ),
    'overtrue/socialite' => 
    array (
      'pretty_version' => '3.5.0',
      'version' => '3.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1607bc8aa95235b1538b3dc4a36324ee2cdf0df5',
    ),
    'overtrue/wechat' => 
    array (
      'pretty_version' => '5.16.4',
      'version' => '5.16.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d8ee0591b8b87558d9c581802bcc31591847743',
    ),
    'phpmentors/workflower' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '8ff9cec155710b88d3d8728b8a1fdcb30615904d',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.21.0',
      'version' => '1.21.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a359d2ccbb89c05f5dffb32711a95f4afc67964',
    ),
    'pimple/pimple' => 
    array (
      'pretty_version' => 'v3.5.0',
      'version' => '3.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a94b3a4db7fb774b3d78dad2315ddc07629e1bed',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '^1.0',
        1 => '1.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0 || 2.0.0 || 3.0.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '^1.0',
        1 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'swoole/ide-helper' => 
    array (
      'pretty_version' => '4.8.6',
      'version' => '4.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b277ed171a29f7bbce1c5feee5581553101e06bf',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v5.1.11',
      'version' => '5.1.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '6def7595e74b4b0a6b515af964792e2d092f056d',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac2e168102a2e06a2624f0379bde94cd5854ced2',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dec8a9f58d20df252b9cd89f1c6c1530f747685d',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '66bea3b09be61613cd3b4043a65a8ec48cfa6d2a',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c68c6d1a308f6e2a1382bdb3a317959e1ee9aa08',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.37',
      'version' => '4.4.37.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b17d76d7ed179f017aad646e858c90a2771af15d',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ef409ff341a565a3663157d4324536746d49a0c7',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '30885182c981ab175d4d034db0f6f469898070ab',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '57b712b08eddb97c762a8caa32c84e037892d2e9',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.37',
      'version' => '4.4.37.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b2d924e5a4cb284f293d5092b1dbf0d364cb8b67',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'v2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '22b37c8a3f6b5d94e9cdbd88e1270d96e2f97b34',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ab11b933cd6bc5464b08e81e2c5b07dec58b0fc',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9dd7403232c61e87e27fb306bbcd1627f245d70',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd28150f0f44ce854e942b671fc2620a98aae1b1e',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '970a01f208bf895c5f327ba40b72288da43adec4',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v5.4.3',
      'version' => '5.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b199936b7365be36663532e547812d3abb10234a',
    ),
    'tencentcloud/tencentcloud-sdk-php' => 
    array (
      'pretty_version' => '3.0.571',
      'version' => '3.0.571.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cb5802d75f1862ed9d22c428238a0ba8cd76ec2',
    ),
    'thans/tp-jwt-auth' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f91eab28e4ec046d96aeb868d40b1b6e2df1c74d',
    ),
    'tightenco/collect' => 
    array (
      'pretty_version' => 'v8.83.0',
      'version' => '8.83.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f8053a9e26ffe0db24db02ef346c40bca920f8f8',
    ),
    'topthink/framework' => 
    array (
      'pretty_version' => 'v6.0.8',
      'version' => '6.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '4789343672aef06d571d556da369c0e156609bce',
    ),
    'topthink/think-helper' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
    ),
    'topthink/think-migration' => 
    array (
      'pretty_version' => 'v3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5717d9e5f3ea745f6dbfd1e30b4402aaadff9a79',
    ),
    'topthink/think-orm' => 
    array (
      'pretty_version' => 'v2.0.36',
      'version' => '2.0.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f48dc09050f25029d41a66bfc9c3c403e4f82024',
    ),
    'topthink/think-queue' => 
    array (
      'pretty_version' => 'v3.0.6',
      'version' => '3.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9f81126bdd52d036461e0c6556592dd478c8728',
    ),
    'topthink/think-template' => 
    array (
      'pretty_version' => 'v2.0.8',
      'version' => '2.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'abfc293f74f9ef5127b5c416310a01fe42e59368',
    ),
    'topthink/think-trace' => 
    array (
      'pretty_version' => 'v1.4',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a9fa8f767b6c66c5a133ad21ca1bc96ad329444',
    ),
    'topthink/think-view' => 
    array (
      'pretty_version' => 'v1.0.14',
      'version' => '1.0.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edce0ae2c9551ab65f9e94a222604b0dead3576d',
    ),
    'uctoo/think-easywechat' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '618313ace2742f45f17a7115c3553e32a10fa2a1',
    ),
    'uctoo/uctoo-api-client-php' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '9c35eb4b593af37e18d5f2856edd6efa37f59cb6',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
    'wechatpay/wechatpay' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd384ebae883f6c9075d11fda4a59e969818b91b7',
    ),
    'wechatpay/wechatpay-guzzle-middleware' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '39f5b4ede49fa97ea712e8ac353c8cda9748f357',
    ),
    'xaboy/form-builder' => 
    array (
      'pretty_version' => '2.0.19',
      'version' => '2.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cf3f5435a63954c1d11ba82c3cd4cfe3d145acf',
    ),
  ),
);







public static function getInstalledPackages()
{
return array_keys(self::$installed['versions']);
}









public static function isInstalled($packageName)
{
return isset(self::$installed['versions'][$packageName]);
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

$ranges = array();
if (isset(self::$installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = self::$installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}





public static function getVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['version'])) {
return null;
}

return self::$installed['versions'][$packageName]['version'];
}





public static function getPrettyVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return self::$installed['versions'][$packageName]['pretty_version'];
}





public static function getReference($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['reference'])) {
return null;
}

return self::$installed['versions'][$packageName]['reference'];
}





public static function getRootPackage()
{
return self::$installed['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
}
}
