<?php
declare(strict_types=1);

/**
 * 在需要支持分组方法的控制器中使用此trait，控制器中必须有user成员变量，且user所赋值的模型需支持UserHasGroupTrait模型
 * 在route.php中添加addGroups、getGroups、delGroups对应的路由，并且注意接口权限
 * @filename  UserHasGroupTrait.php
 * @createdAt 2022/2/22
 * @project  https://gitee.com/uctoo/uctoo
 * @document https://gitee.com/uctoo/uctoo
 * @author   UCToo <contact@uctoo.com>
 * @copyright By UCToo
 * @license  https://gitee.com/uctoo/uctoo/raw/master/license.txt
 */
namespace catcher\traits;

use catcher\CatchResponse;
use catcher\Code;

trait UserHasGroupTrait
{

    /**
     * 加入用户组
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param int user_id 用户表id,可选参数，高优先级获取用户
     * @param string token 用户表token，可选参数，低优先级获取用户，即当前用户加入groups用户组
     * @param string groups 用户组id，以逗号分隔
     * @return \think\response\Json  返回新增的中间表数据
     */
    public function addGroups(\think\Request $request)
    {
        $user_id = input('user_id');
        $token = $request->header('token');
        $groups = $request->param('groups');
        if(!$groups){
            return CatchResponse::fail('无有效用户组', Code::PARAM_ERROR);
        }
        if(isset($user_id)){
            $user = $this->user->findBy($user_id);
        }elseif (isset($token)){
            $user = $this->user->where('token','=',$token)->find();
        }
        if(!$user){
            return CatchResponse::fail('无有效用户', Code::PARAM_ERROR);
        }

        $hasGroup = $user->getGroups()->toArray();
        $hasGroupIds = array_column($hasGroup,'id');
        $addGroup = explode(',',$groups);
        $diff = array_diff($addGroup,$hasGroupIds);  //只添加差集，以使中间表无多余数据
        $res = $user->attachGroups($diff);
        return CatchResponse::success($res);
    }

    /**
     * 用户所在组
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param int user_id 用户表id，可选参数，高优先级获取用户
     * @param string token 用户表token，可选参数，低优先级获取用户，即当前用户加入groups用户组
     * @return \think\response\Json
     */
    public function getGroups(\think\Request $request)
    {
        $user_id = input('user_id');
        $token = $request->header('token');
        if(isset($user_id)){
            $user = $this->user->findBy($user_id);
        }elseif (isset($token)){
            $user = $this->user->where('token','=',$token)->find();
        }
        if(!$user){
            return CatchResponse::fail('无有效用户', Code::PARAM_ERROR);
        }
        $res = $user->getGroups();
        return CatchResponse::success($res);
    }

    /**
     * 退出用户组
     *
     * @time 2022年02月22日
     * @param \think\Request $request
     * @param int user_id 用户表id，可选参数，高优先级获取用户
     * @param string token 用户表token，可选参数，低优先级获取用户，即当前用户加入groups用户组
     * @param string groups 用户组id，以逗号分隔
     * @return \think\response\Json
     */
    public function delGroups(\think\Request $request)
    {
        $user_id = input('user_id');
        $token = $request->header('token');
        $groups = $request->param('groups');
        if(!$groups){
            return CatchResponse::fail('无有效用户组', Code::PARAM_ERROR);
        }
        if(isset($user_id)){
            $user = $this->user->findBy($user_id);
        }elseif (isset($token)){
            $user = $this->user->where('token','=',$token)->find();
        }
        if(!$user){
            return CatchResponse::fail('无有效用户', Code::PARAM_ERROR);
        }
        $res = $user->detachGroups(explode(',',$groups));
        return CatchResponse::success($res);
    }
}