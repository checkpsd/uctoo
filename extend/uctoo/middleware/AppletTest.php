<?php
declare (strict_types = 1);

namespace uctoo\middleware;

use uctoo\util\library\Mpopen;
use EasyWeChat\Factory;

class AppletTest
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $applet = \catchAdmin\wechatopen\model\Applet::appInfo($request->user['aid']);
        if(!$applet){
            abort(404);
        }
        $app = invoke(Mpopen::class);
        $mapp = $app->miniProgram($applet['appid'],$applet['refresh_token']);
        $mapp->config->add('env',$applet['env']);
        $mapp->rebind('cache',app('cache'));
        $request->applet = $applet;
        $request->mapp = $mapp;

        return $next($request);
    }
}
