<?php
declare (strict_types = 1);

namespace uctoo\middleware;

//use app\model\saas\MerchantAccount;
use uctoo\util\exception\TokenException;
use think\exception\HttpResponseException;
use think\Response;

class Auth
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $auth = \uctoo\util\library\Auth::instance();
        $auth->setAccessToken($request->header('authorization',null));
        $request->auth = $auth;

        try{
            $user = $request->auth->checkUser();
        }catch (TokenException $e){
            $result = [
                'code' => $e->getCode(),
                'msg'  => $e->getMessage(),
                'time' => $request->server('REQUEST_TIME'),
                'data' => null,
            ];
            return Response::create($result, 'json', 200);

            //测试环境
//            $user = [
//                'uid' => 12523,
//                'aid' => 'wx395b95e36c36cee5'
//            ];
        }

        if(!$user['aid']){
            //查询是否已经授权
            $user['aid'] = MerchantAccount::where('id',$user['uid'])->value('applet_appid');
        }
        $request->user = $user;
        return $next($request);
    }
}
