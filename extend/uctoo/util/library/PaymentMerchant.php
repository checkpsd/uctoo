<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\library;


use uctoo\ThinkEasyWeChat\PaymentMerchant\Application;
use think\facade\Config;

class PaymentMerchant
{
    public static function instance(){
        $app =  new Application(Config::get('micro_merchant'));//TODO:从管理后台配置获取
        $app->rebind('cache',app('cache'));
        return $app;
    }
}